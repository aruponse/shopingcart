package com.pruebas.cart.controller

import com.pruebas.cart.model.Producto
import com.pruebas.cart.repository.ProductoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.ObjectUtils
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.util.*

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
class ProductoController @Autowired constructor (val productoRepository: ProductoRepository){

    @GetMapping("/api/productos")
    fun index(): ResponseEntity<List<Producto>>{
        val productos = productoRepository.findAll()
        if(productos.isEmpty()){
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(productos, HttpStatus.OK)
    }

    @GetMapping("/api/producto/{id}")
    fun fetchProductoById(@PathVariable("id") id: String): ResponseEntity<Producto> {
        val producto = productoRepository.findById(UUID.fromString(id))
        if (producto.isPresent) {
            return ResponseEntity(producto.get(), HttpStatus.OK)
        }
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @GetMapping("/api/producto/{nombre}/{sku}")
    fun fetchProductoByNombreOrSku(@PathVariable("nombre") nombre: String, @PathVariable("sku") sku: String): ResponseEntity<List<Producto>> {
        var productos: List<Producto>
        if(StringUtils.isEmpty(sku) && !StringUtils.isEmpty(nombre)) {
            productos = productoRepository.findByNombre(nombre)
        }else{
            if(!StringUtils.isEmpty(sku) && StringUtils.isEmpty(nombre)) {
                productos = productoRepository.findBySku(sku)
            }
        }

        if(!StringUtils.isEmpty(sku) && !StringUtils.isEmpty(nombre)) {
            productos = productoRepository.findByNombreOrSku(nombre, sku)
        }else{
            productos = productoRepository.findAll()
        }

        if (ObjectUtils.isEmpty(productos)) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
        return ResponseEntity(productos, HttpStatus.OK)
    }

    @PostMapping("/api/producto")
    fun addNewProducto(@RequestBody producto: Producto,
                       uri: UriComponentsBuilder): ResponseEntity<Producto> {

        val persistedObj = productoRepository.save(producto)
        if (ObjectUtils.isEmpty(persistedObj)) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        val headers = HttpHeaders()
        headers.setLocation(uri.path("/api/producto/{id}").buildAndExpand(producto.id).toUri());
        return ResponseEntity(headers, HttpStatus.CREATED)
    }

    @PutMapping("/api/producto/{id}")
    fun editProducto(@PathVariable("id") id: UUID,
                     @RequestBody producto: Producto,
                     uri: UriComponentsBuilder ): ResponseEntity<Producto> {
        if(ObjectUtils.isEmpty(producto) || StringUtils.isEmpty(id)){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        return productoRepository.findById(id).map{
            nuevoProducto ->
                val editedObj: Producto = nuevoProducto.copy(
                        id = id,
                        nombre = producto.nombre,
                        sku = producto.sku,
                        descripcion = producto.descripcion,
                        precio = producto.precio,
                        tipoProducto = producto.tipoProducto
                )
            ResponseEntity(productoRepository.save(editedObj), HttpStatus.OK)
        }.orElse(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))
    }

    @DeleteMapping("/api/producto/{id}")
    fun removeProductoById(@PathVariable("id") id: UUID,
                        uri: UriComponentsBuilder): ResponseEntity<Void>{
        val producto = productoRepository.findById(id)
        if(producto.isPresent()){
            productoRepository.deleteById(id)
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}