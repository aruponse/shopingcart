package com.pruebas.cart.controller

import com.pruebas.cart.model.Cart
import com.pruebas.cart.model.enums.EstadoCarritoEnum
import com.pruebas.cart.repository.CartDetailsRepository
import com.pruebas.cart.repository.CartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.ObjectUtils
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.util.*

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
class CartController
    @Autowired constructor(val cartRepository: CartRepository,
                           val cartDetailRepository: CartDetailsRepository){

    @GetMapping("/api/carts")
    fun index(): ResponseEntity<List<Cart>>{
        val carts = cartRepository.findAllByEstado(EstadoCarritoEnum.C)
        if(carts.isEmpty()){
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(carts, HttpStatus.OK)
    }

    @GetMapping("/api/cart")
    fun fetchActiveCart(): ResponseEntity<Optional<Cart>>{
        val cart = cartRepository.findByEstado(EstadoCarritoEnum.P)
        if(cart.isPresent){
            return ResponseEntity(cart, HttpStatus.OK)
        }
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @GetMapping("/api/cart/{id}")
    fun fetchCartById(@PathVariable("id") id: String): ResponseEntity<Cart> {
        val cart = cartRepository.findById(UUID.fromString(id))
        if (cart.isPresent) {
            return ResponseEntity(cart.get(), HttpStatus.OK)
        }
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @PostMapping("/api/cart")
    fun addNewCart(@RequestBody cart: Cart, uri: UriComponentsBuilder): ResponseEntity<Cart> {
        val validatorObj = cartRepository.findByEstado(EstadoCarritoEnum.P)
        if(validatorObj.isPresent){
            return ResponseEntity(validatorObj.get(), HttpStatus.NOT_ACCEPTABLE)
        }
        val persistedObj = cartRepository.save(cart)
        if (ObjectUtils.isEmpty(persistedObj)) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        val headers = HttpHeaders()
        headers.setLocation(uri.path("/api/cart/{id}").buildAndExpand(cart.id).toUri());
        return ResponseEntity(headers, HttpStatus.CREATED)
    }

    @PutMapping("/api/cart/{id}")
    fun editCart(@PathVariable("id") id: UUID,
                     @RequestBody cart: Cart,
                     uri: UriComponentsBuilder ): ResponseEntity<Cart> {
        if(ObjectUtils.isEmpty(cart) || StringUtils.isEmpty(id)){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        return cartRepository.findById(id).map{
            nuevoCart ->
                val editedObj: Cart = nuevoCart.copy(
                        id = id,
                        total = cart.total,
                        estado = cart.estado
                )
            ResponseEntity(cartRepository.save(editedObj), HttpStatus.OK)
        }.orElse(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))
    }

    @DeleteMapping("/api/cart/{id}")
    fun removeCartById(@PathVariable("id") id: UUID,
                        uri: UriComponentsBuilder): ResponseEntity<Void>{
        val cart = cartRepository.findById(id)
        if(cart.isPresent()){
            cartRepository.deleteById(id)
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @GetMapping("/api/checkout/{cartId}")
    fun checkout(@PathVariable cartId: UUID): ResponseEntity<Cart>{
        var total: Double = 0.0
        if(StringUtils.isEmpty(cartId)){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        cartDetailRepository.findAllByCartId(cartId).forEach{
                newDetail ->
            total += newDetail.cantidad * newDetail.precio
        }
        return cartRepository.findById(cartId).map{
                nuevoCart ->
            val editedObj: Cart = nuevoCart.copy(
                id = cartId,
                total = total,
                estado = EstadoCarritoEnum.C
            )
            ResponseEntity(cartRepository.save(editedObj), HttpStatus.OK)
        }.orElse(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))

    }
}