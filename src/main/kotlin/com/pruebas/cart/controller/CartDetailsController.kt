package com.pruebas.cart.controller

import com.pruebas.cart.model.Cart
import com.pruebas.cart.model.CartDetails
import com.pruebas.cart.model.Producto
import com.pruebas.cart.model.dto.CartDetailsDTO
import com.pruebas.cart.model.enums.EstadoCarritoEnum
import com.pruebas.cart.model.enums.TipoProductoEnum
import com.pruebas.cart.repository.CartDetailsRepository
import com.pruebas.cart.repository.CartRepository
import com.pruebas.cart.repository.ProductoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.NumberUtils
import org.springframework.util.ObjectUtils
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import java.util.*

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
class CartDetailsController
@Autowired constructor(val cartDetailRepository: CartDetailsRepository,
                       val cartRepository: CartRepository,
                       val productoRepository: ProductoRepository){

    @GetMapping("/api/cartDetails")
    fun index(): ResponseEntity<List<CartDetails>>{
        val cartDetails = cartDetailRepository.findAll()
        if(cartDetails.isEmpty()){
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(cartDetails, HttpStatus.OK)
    }

    @GetMapping("/api/cartDetail/{id}")
    fun fetchCartDetailsById(@PathVariable("id") id: String): ResponseEntity<CartDetails> {
        val cartDetail = cartDetailRepository.findById(UUID.fromString(id))
        if (cartDetail.isPresent) {
            return ResponseEntity(cartDetail.get(), HttpStatus.OK)
        }
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @GetMapping("/api/cartDetails/{id}")
    fun fetchCartDetailsByCartId(@PathVariable("id") id: String): ResponseEntity<List<CartDetails>> {
        val cartDetail = cartDetailRepository.findAllByCartId(UUID.fromString(id))
        if (cartDetail.isEmpty()) {
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(cartDetail, HttpStatus.OK)
    }

    @PostMapping("/api/cartDetail")
    fun addNewCartDetails(@RequestBody cartDetail: CartDetailsDTO,
                          uri: UriComponentsBuilder): ResponseEntity<CartDetails> {

        var currentCarts: Optional<Cart> = Optional.empty()
        if(StringUtils.isEmpty(cartDetail.cartId)) {
            currentCarts = cartRepository.findByEstado(EstadoCarritoEnum.P)
        }
        val currentProductos = productoRepository.findById(UUID.fromString(cartDetail.productoId))
        var currentCart: Cart
        var currentProducto: Producto
        if(currentCarts.isPresent){
            currentCart = currentCarts.get()
        }else{
            if(StringUtils.isEmpty(cartDetail.cartId))
                return ResponseEntity(HttpStatus.BAD_REQUEST)
            else {
                currentCarts = cartRepository.findById(UUID.fromString(cartDetail.cartId))
                if(currentCarts.isPresent)
                    currentCart = currentCarts.get()
                else
                    return ResponseEntity(HttpStatus.BAD_REQUEST)
            }
        }
        if(currentProductos.isPresent) {
            currentProducto = currentProductos.get()
            val currentDetails = cartDetailRepository.findByCartIdAndProductoId(currentCart.id, currentProducto.id)
            if(currentDetails.isPresent){
                val currentDetail = currentDetails.get()
                return currentDetails.map{
                        nuevoCartDetails ->
                    val editedObj: CartDetails = nuevoCartDetails.copy(
                        id = currentDetail.id,
                        cartId = currentDetail.cartId,
                        productoId = currentDetail.productoId,
                        cantidad = cartDetail.cantidad + currentDetail.cantidad,
                        precio = cartDetail.precio
                    )
                    ResponseEntity(cartDetailRepository.save(editedObj), HttpStatus.OK)
                }.orElse(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))

            }
        }else
            return ResponseEntity(HttpStatus.BAD_REQUEST)

        val precio: Double
        if(currentProducto.tipoProducto == TipoProductoEnum.D){
            precio = currentProducto.precio / 2
        }else{
            precio = currentProducto.precio
        }
        val cartDetailToPresist = CartDetails(
            id = UUID.randomUUID(),
            cartId = currentCart.id,
            productoId = currentProducto.id,
            cantidad = cartDetail.cantidad,
            precio = precio
        )

        val persistedObj = cartDetailRepository.save(cartDetailToPresist)
        if (ObjectUtils.isEmpty(persistedObj)) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        val headers = HttpHeaders()
        headers.setLocation(uri.path("/api/cartDetail/{id}").buildAndExpand(cartDetailToPresist.id).toUri());
        return ResponseEntity(headers, HttpStatus.CREATED)
    }

    @PutMapping("/api/cartDetail/{id}")
    fun editCartDetails(@PathVariable("id") id: UUID,
                     @RequestBody cartDetail: CartDetailsDTO,
                     uri: UriComponentsBuilder ): ResponseEntity<CartDetails> {
        if(ObjectUtils.isEmpty(cartDetail) || StringUtils.isEmpty(id)
            || StringUtils.isEmpty(cartDetail.cartId) || StringUtils.isEmpty(cartDetail.productoId)){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
        return cartDetailRepository.findById(id).map{
            nuevoCartDetails ->
                val editedObj: CartDetails = nuevoCartDetails.copy(
                    id = id,
                    cartId = UUID.fromString(cartDetail.cartId),
                    productoId = UUID.fromString(cartDetail.productoId),
                    cantidad = cartDetail.cantidad,
                    precio = cartDetail.precio
                )
            ResponseEntity(cartDetailRepository.save(editedObj), HttpStatus.OK)
        }.orElse(ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR))
    }

    @DeleteMapping("/api/cartDetail/{id}")
    fun removeCartDetailsById(@PathVariable("id") id: UUID,
                        uri: UriComponentsBuilder): ResponseEntity<Void>{
        val cartDetail = cartDetailRepository.findById(id)
        if(cartDetail.isPresent()){
            cartDetailRepository.deleteById(id)
            return ResponseEntity(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}