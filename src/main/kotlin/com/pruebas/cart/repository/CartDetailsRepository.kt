package com.pruebas.cart.repository

import com.pruebas.cart.model.CartDetails
import com.pruebas.cart.model.enums.EstadoCarritoEnum
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CartDetailsRepository: JpaRepository<CartDetails, UUID> {

    fun findAllByCartId(cartId: UUID): List<CartDetails>
    fun findByCartIdAndProductoId(cartId: UUID, productoId: UUID): Optional<CartDetails>

}