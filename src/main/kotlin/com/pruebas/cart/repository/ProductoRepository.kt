package com.pruebas.cart.repository

import com.pruebas.cart.model.Producto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ProductoRepository: JpaRepository<Producto, UUID> {

    fun findByNombre(nombre: String): List<Producto>
    fun findBySku(sku: String): List<Producto>
    fun findByNombreOrSku(nombre: String, sku: String): List<Producto>
    fun findOneById(id: UUID): Producto

}