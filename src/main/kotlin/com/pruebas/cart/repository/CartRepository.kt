package com.pruebas.cart.repository

import com.pruebas.cart.model.Cart
import com.pruebas.cart.model.enums.EstadoCarritoEnum
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CartRepository: JpaRepository<Cart, UUID> {

    fun findByEstado(estado: EstadoCarritoEnum): Optional<Cart>
    fun findAllByEstado(estado: EstadoCarritoEnum): List<Cart>
    fun findOneByEstado(estado: EstadoCarritoEnum): Cart

}