package com.pruebas.cart.model

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "cart_details")
data class CartDetails(
    @Id
    val id: UUID = UUID.randomUUID(),
    @Column("cart_id", nullable = true)
    var cartId: UUID,
    @Column("producto_id", nullable = true)
    var productoId: UUID,
    @Column
    val cantidad: Double,
    @Column
    val precio: Double
        ) {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "producto_id", referencedColumnName = "id", insertable = false, updatable = false)
    val producto: Producto? = null

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cart_id", referencedColumnName = "id", insertable = false, updatable = false)
    val cart: Cart? =  null

}