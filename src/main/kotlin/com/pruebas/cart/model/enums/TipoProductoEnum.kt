package com.pruebas.cart.model.enums

enum class TipoProductoEnum(val tipo: String) {
    S("Simple"),
    D("Con descuento")
}