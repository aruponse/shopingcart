package com.pruebas.cart.model.enums

enum class EstadoCarritoEnum(val estado: String) {
    P("Pendiente"),
    C("Completado")
}