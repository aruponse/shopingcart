package com.pruebas.cart.model.dto

import java.util.*

data class CartDetailsDTO(
    val cartId: String? = null,
    val productoId: String? = null,
    val cantidad: Double,
    val precio: Double
)