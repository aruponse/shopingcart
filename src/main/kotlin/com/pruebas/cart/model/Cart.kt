package com.pruebas.cart.model

import com.pruebas.cart.model.enums.EstadoCarritoEnum
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "cart")
data class Cart (
    @Id
    val id: UUID = UUID.randomUUID(),
    @Column("total")
    val total: Double = 0.0,
    @Enumerated(EnumType.STRING)
    @Column("estado")
    val estado: EstadoCarritoEnum = EstadoCarritoEnum.P
){

}