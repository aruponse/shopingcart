package com.pruebas.cart.model

import com.pruebas.cart.model.enums.TipoProductoEnum
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "PRODUCTO")
data class Producto (
    @Id
    val id: UUID = UUID.randomUUID(),
    @Column(name = "nombre")
    val nombre: String,
    @Column(name = "sku")
    val sku: String,
    @Column(name = "descripcion")
    val descripcion: String,
    @Column(name = "precio")
    val precio: Double,
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_producto")
    val tipoProducto: TipoProductoEnum
)